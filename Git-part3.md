# Query: ###
# ContextLines: 1

# Анализ сделанных изменений

Во время разработки программистам часто приходится останавливаться и анализировать изменения, которые они сделали с последнего коммита. Потребность смотреть изменения становится очевидной, если представить себе, что такое работа над реальным проектом. Как правило, это тысячи (десятки и сотни тысяч) строк кода, сотни и тысячи файлов и иногда несколько дней работы. Потратив даже несколько часов над работой в таком проекте, очень сложно вспомнить что и где менялось, а что ещё осталось поменять.

### Анализировать изменения важно даже в небольших проектах. Прямо сейчас во время разработки этого курса изменилось несколько файлов и git status выглядит так:
```
$ git status


Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
    modified:   300-working-directory/README.md

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
    modified:   100-intro/README.md
    modified:   250-github/README.md
    modified:   300-working-directory/README.md
    modified:   300-working-directory/spec.yml
    modified:   350-changes/README.md
```
### Попробуем воспроизвести подобную ситуацию в нашем проекте. Выполним следующий код в репозитории myrepo-git:
```
myrepo-git$ echo 'new line' >> INFO.md
myrepo-git$ echo 'Hello, World! How are you?' > README.md

myrepo-git$ git status

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
    modified:   INFO.md
    modified:   README.md

no changes added to commit (use "git add" and/or "git commit -a")
```
### Изменились оба файла. В один мы добавили строчку, в другом заменили. Как теперь посмотреть эти изменения? Для этого в git есть команда git diff, которая показывает разницу между тем, что было и что стало:

#### Работа git diff
```
myrepo-git$ git diff

diff --git a/INFO.md b/INFO.md
index d5225f8..40f51f1 100644
--- a/INFO.md
+++ b/INFO.md
@@ -1 +1,2 @@

 git is awesome!
+new line

diff --git a/README.md b/README.md
index ffe7ece..00fd294 100644
--- a/README.md
+++ b/README.md
@@ -1 +1 @@

-Hello, World!
+Hello, World! How are you?
```
### git diff выводит именно те строки, которые изменились (и иногда строки вокруг измененных для удобства анализа), а не файлы целиком. Слева от них ставится знак "-", если строка была удалена, и "+" для добавленных строк.

#### По умолчанию git diff показывает изменения только для тех модифицированных файлов, которые ещё не были добавлены в индекс. Подразумевается, что добавленные в индекс файлы смотреть не нужно, ведь мы их уже подготовили к коммиту. 
#### В реальности же часто хочется и, более того, нужно увидеть эти изменения. Для этого нужно запустить команду вывода дифа с флагом --staged:

#### Выведет все изменения сделанные в рабочей директории которые были добавлены в индекс
```
myrepo-git$ git diff --staged
```

### git diff — команда, которую нужно обязательно запускать перед каждым коммитом. Она позволяет проанализировать добавляемые изменения и исправить возможные ошибки. 
Самостоятельная работа

    Выполните все шаги из урока
    Зафиксируйте все изменения в репозитории. Сделайте коммит с сообщением add fix
    Залейте изменения на Gitlab
